<?php
/**
 * @file
 * The primary PHP file for this theme.
 */
function esace_2016_preprocess_page(&$variables) {
  $key = array_keys($variables['navbar_classes_array'], 'container');
  unset($variables['navbar_classes_array'][$key[0]]);

  if (isset($variables['node']->type)) {
      $nodetype = $variables['node']->type;
      $variables['theme_hook_suggestions'][] = 'page__' . $nodetype;
  }
}

