<?php
/**
 * @file
 * Default theme implementation to display a single Drupal page.
 *
 * The doctype, html, head and body tags are not in this template. Instead they
 * can be found in the html.tpl.php template in this directory.
 *
 * Available variables:
 *
 * General utility variables:
 * - $base_path: The base URL path of the Drupal installation. At the very
 *   least, this will always default to /.
 * - $directory: The directory the template is located in, e.g. modules/system
 *   or themes/bartik.
 * - $is_front: TRUE if the current page is the front page.
 * - $logged_in: TRUE if the user is registered and signed in.
 * - $is_admin: TRUE if the user has permission to access administration pages.
 *
 * Site identity:
 * - $front_page: The URL of the front page. Use this instead of $base_path,
 *   when linking to the front page. This includes the language domain or
 *   prefix.
 * - $logo: The path to the logo image, as defined in theme configuration.
 * - $site_name: The name of the site, empty when display has been disabled
 *   in theme settings.
 * - $site_slogan: The slogan of the site, empty when display has been disabled
 *   in theme settings.
 *
 * Navigation:
 * - $main_menu (array): An array containing the Main menu links for the
 *   site, if they have been configured.
 * - $secondary_menu (array): An array containing the Secondary menu links for
 *   the site, if they have been configured.
 * - $breadcrumb: The breadcrumb trail for the current page.
 *
 * Page content (in order of occurrence in the default page.tpl.php):
 * - $title_prefix (array): An array containing additional output populated by
 *   modules, intended to be displayed in front of the main title tag that
 *   appears in the template.
 * - $title: The page title, for use in the actual HTML content.
 * - $title_suffix (array): An array containing additional output populated by
 *   modules, intended to be displayed after the main title tag that appears in
 *   the template.
 * - $messages: HTML for status and error messages. Should be displayed
 *   prominently.
 * - $tabs (array): Tabs linking to any sub-pages beneath the current page
 *   (e.g., the view and edit tabs when displaying a node).
 * - $action_links (array): Actions local to the page, such as 'Add menu' on the
 *   menu administration interface.
 * - $feed_icons: A string of all feed icons for the current page.
 * - $node: The node object, if there is an automatically-loaded node
 *   associated with the page, and the node ID is the second argument
 *   in the page's path (e.g. node/12345 and node/12345/revisions, but not
 *   comment/reply/12345).
 *
 * Regions:
 * - $page['help']: Dynamic help text, mostly for admin pages.
 * - $page['highlighted']: Items for the highlighted content region.
 * - $page['content']: The main content of the current page.
 * - $page['sidebar_first']: Items for the first sidebar.
 * - $page['sidebar_second']: Items for the second sidebar.
 * - $page['header']: Items for the header region.
 * - $page['footer']: Items for the footer region.
 *
 * @see bootstrap_preprocess_page()
 * @see template_preprocess()
 * @see template_preprocess_page()
 * @see bootstrap_process_page()
 * @see template_process()
 * @see html.tpl.php
 *
 * @ingroup templates
 */

// var DEV
$urlFull = $GLOBALS['base_url'] . '/' . drupal_get_path('theme', 'esace_2016');

?>
<?php //dsm('page--simplenews.tpl.php'); ?>
<?php //print 'page--simplenews.tpl.php'; ?>

<table width="100%" height="100%" cellpadding="0" cellspacing="0" border="0" bgcolor="#f3f3f3">
<tr>
  <td width="100%" valign="top" align="center">

    <div style="display: none; font-size: 1px; line-height: 1px; max-height: 0px; max-width: 0px; opacity: 0; overflow: hidden;">
      <?php print $title; ?>
    </div>

    <center>
      <table border="0" cellpadding="0" cellspacing="0" height="100%" width="100%">

        <!-- START HERO -->
        <tr>
          <td align="center" height="100%" valign="top" width="100%">
            <!--[if (gte mso 9)|(IE)]>
            <table align="center" border="0" cellspacing="0" cellpadding="0" width="660">
              <tr>
                <td align="center" valign="top" width="660">
                  <![endif]-->
                  <table align="center" border="0" cellpadding="0" cellspacing="0" width="100%" style="max-width:660px;">
                    <tr>
                      <td background="images/hero.png" bgcolor="#f3f3f3" width="660" height="442" valign="top">
                        <!--[if gte mso 9]>
                        <v:rect xmlns:v="urn:schemas-microsoft-com:vml" fill="true" stroke="false" style="width:600px;height:442px;">
                          <v:fill type="tile" src="images/hero.png" color="#FFFFFF" />
                          <v:textbox inset="0,0,0,0">
                            <![endif]-->
                            <div>
                              <table width="100%" height="100%" border="0" cellpadding="0" cellspacing="0">
                                <tr>
                                  <td align="center" height="100%" valign="top" width="100%" style="padding:0 20px;">
                                    <!--[if (gte mso 9)|(IE)]>
                                    <table align="center" border="0" cellspacing="0" cellpadding="0" width="660">
                                      <tr>
                                        <td align="center" valign="top" width="660">
                                          <![endif]-->
                                          <table align="center" border="0" cellpadding="0" cellspacing="0" width="100%" style="max-width:660px;">
                                            <tr>
                                              <td height="20" style="font-size: 20px; line-height: 20px;">&nbsp;</td>
                                            </tr>
                                            <tr>
                                              <td align="center" style="font-family: 'Montserrat', Arial, sans-serif; font-size:10px; line-height:14px; color:#666666;" class="body-text">
                                                <p style="font-family: 'Montserrat', Arial, sans-serif; font-size:10px; line-height:14px; color:#666666; padding:0; margin:0;" class="body-text">Se você não conseguir visualizar esse email, <a href="<?php print $node_url; ?>" target="_blank" style="font-family: 'Montserrat', Arial, sans-serif; font-size:10px; line-height:14px; color:#364053; padding:0; margin:0;" class="body-text">clique aqui.</a></p>
                                            </tr>
                                            <tr>
                                              <td height="40" style="font-size: 40px; line-height: 40px;">&nbsp;</td>
                                            </tr>
                                            <tr>
                                              <td align="center" valign="top">
                                                <a href="http://esace.org.br" target="_blank" title="Ir para o Portal"><img src="<?php print $logo; ?>" width="110" height="75" style="margin:0; padding:0; border:none; display:block;" border="0" alt="Logo ESA-CE"></a>
                                              </td>
                                            </tr>
                                            <tr>
                                              <td height="35" style="font-size: 35px; line-height: 35px;">&nbsp;</td>
                                            </tr>
                                            <tr>
                                              <td align="center" style="font-family: 'Montserrat', Arial, sans-serif; font-size:30px; line-height:36px; font-weight:bold; color:#ffffff; text-transform:uppercase;" class="body-text">
                                                <h1 style="font-family: 'Montserrat', Arial, sans-serif; font-size:30px; line-height:36px; font-weight:bold; color:#364053; text-transform:uppercase; padding:0; margin:0;" class="body-text">Cursos online e presencial</h1>
                                              </td>
                                            </tr>
                                            <tr>
                                              <td height="20" style="font-size: 20px; line-height: 20px;">&nbsp;</td>
                                            </tr>
                                            <tr>
                                              <td style="font-family: 'Montserrat', Arial, sans-serif; font-size:14px; line-height:20px; color:#757575;" align="center" class="body-text">
                                                <p style="font-family: 'Montserrat', Arial, sans-serif; font-size:14px; line-height:20px; color:#757575; padding:0; margin:0;" align="center" class="body-text">Escola Superior de Advocacia do Ceará atua como entidade parceira e colaboradora da OAB/CE. Realiza, assim, cursos de diversas natureza, de curta e longa duração e de atualização jurídica
                                                </p>
                                              </td>
                                            </tr>
                                            <tr>
                                              <td height="20" style="font-size: 20px; line-height: 20px;">&nbsp;</td>
                                            </tr>
                                            <tr>
                                              <td align="center">
                                                <table border="0" cellspacing="0" cellpadding="0" align="center">
                                                  <tr>
                                                    <td>
                                                      <table border="0" cellspacing="0" cellpadding="0" align="center">
                                                        <tr>
                                                          <td align="center" style="-webkit-border-radius: 50px; -moz-border-radius: 50px; border-radius: 50px;" bgcolor="#26a4d3" class="body-text">
                                                            <a href="http://esace.org.br/" target="_blank" style="font-size: 14px; font-family: 'Montserrat', Arial, sans-serif; color: #ffffff; text-decoration: none; text-decoration: none; border-radius: 50px; padding: 12px 22px; border: 1px solid #26a4d3; display: inline-block; text-transform:uppercase; font-weight:bold;" class="body-text">Acesse o portal</a>
                                                          </td>
                                                        </tr>
                                                      </table>
                                                    </td>
                                                  </tr>
                                                </table>
                                              </td>
                                            </tr>
                                          </table>
                                          <!--[if (gte mso 9)|(IE)]>
                                        </td>
                                      </tr>
                                    </table>
                                    <![endif]-->
                                  </td>
                                </tr>
                              </table>
                            </div>
                            <!--[if gte mso 9]>
                          </v:textbox>
                        </v:rect>
                        <![endif]-->
                      </td>
                    </tr>
                  </table>
                  <!--[if (gte mso 9)|(IE)]>
                </td>
              </tr>
            </table>
            <![endif]-->
          </td>
        </tr>
        <!-- END HERO -->

        <!-- START INTRO -->
        <tr>
          <td>
            <table cellspacing="0" cellpadding="0" border="0" bgcolor="#ffffff" width="100%" style="max-width: 660px;" align="center">
              <tr>
                <td height="40" style="font-size: 40px; line-height: 40px;">&nbsp;</td>
              </tr>
              <tr>
                <td align="left" valign="top" style="font-family: 'Montserrat', Arial, sans-serif; font-size:20px; line-height:26px; color:#222222; font-weight:bold; text-transform:uppercase;" class="body-text">
                  <h2 style="font-family: 'Montserrat', Arial, sans-serif; font-size:20px; line-height:26px; color:#222222; font-weight:bold; text-transform:uppercase; padding:0 20px; margin:0;" class="body-text"><?php print $title; ?></h2>
                </td>
              </tr>
                <tr>
                <td height="40" style="font-size: 40px; line-height: 40px;">&nbsp;</td>
              </tr>
              <!-- END INTRO -->

              <?php print render($page['content']); ?>

              <!-- START FOOTER BAR -->
              <tr>
                <td align="center" height="100%" valign="top" width="100%" bgcolor="#364053">
                  <!--[if (gte mso 9)|(IE)]>
                  <table align="center" border="0" cellspacing="0" cellpadding="0" width="660">
                    <tr>
                      <td align="center" valign="top" width="660">
                        <![endif]-->
                        <table align="center" border="0" cellpadding="0" cellspacing="0" width="100%" style="max-width:660px;">
                          <tr>
                            <td height="40" style="font-size: 40px; line-height: 40px;">&nbsp;</td>
                          </tr>
                          <tr>
                            <td align="center" valign="top" style="font-size:0; padding:0 20px;">
                              <!--[if (gte mso 9)|(IE)]>
                              <table align="center" border="0" cellspacing="0" cellpadding="0" width="660">
                                <tr>
                                  <td align="center" valign="top" width="660">
                                    <![endif]-->
                                    <table align="center" border="0" cellpadding="0" cellspacing="0" width="100%" style="max-width:660px;">
                                      <tr>
                                        <td align="center" valign="top">
                                          <table width="160" cellpadding="0" cellspacing="0" border="0" class="right">
                                            <tr>
                                              <td width="40" align="center">
                                                <a href="http://facebook.com/esaceara" target="_blank" title="Curta nossa página no Facebook"><img src="<?php print $urlFull; ?>/image/newsletter/facebook.png" width="28" height="29" style="margin:0; padding:0; border:none; display:block;" border="0" alt="Facebook"></a>
                                              </td>
                                              <td width="40" align="center">
                                                <a href="http://twitter.com/esaceara" target="_blank" title="Siga-nos no Twitter"><img src="<?php print $urlFull; ?>/image/newsletter/twitter.png" width="28" height="29" style="margin:0; padding:0; border:none; display:block;" border="0" alt="Twitter"></a>
                                              </td>
                                              <td width="40" align="center">
                                                <a href="https://www.instagram.com/esaceara" target="_blank" title="Siga-nos no Instagram"><img src="<?php print $urlFull; ?>/image/newsletter/instagram.png" width="28" height="29" style="margin:0; padding:0; border:none; display:block;" border="0" alt="Instagram"></a>
                                              </td>
                                            </tr>
                                          </table>
                                        </td>
                                      </tr>
                                    </table>
                                    <!--[if (gte mso 9)|(IE)]>
                                  </td>
                                </tr>
                              </table>
                              <![endif]-->
                            </td>
                          </tr>
                          <tr>
                            <td height="40" style="font-size: 40px; line-height: 40px;">&nbsp;</td>
                          </tr>
                        </table>
                        <!--[if (gte mso 9)|(IE)]>
                      </td>
                    </tr>
                  </table>
                  <![endif]-->
                </td>
              </tr>
              <!-- END FOOTER BAR -->

              <!-- START FOOTER INFO -->
              <tr>
                <td align="center" height="100%" valign="top" width="100%" bgcolor="#FFFFFF">
                  <!--[if (gte mso 9)|(IE)]>
                  <table align="center" border="0" cellspacing="0" cellpadding="0" width="660">
                    <tr>
                      <td align="center" valign="top" width="660">
                        <![endif]-->
                        <table align="center" border="0" cellpadding="0" cellspacing="0" width="100%" style="max-width:660px;">
                          <tr>
                            <td height="20" style="font-size: 20px; line-height: 20px;">&nbsp;</td>
                          </tr>
                          <tr>
                            <td align="center" style="font-family: 'Montserrat', Arial, sans-serif; font-size:10px; line-height:14px; color:#666666;" class="body-text">
                              <p style="font-family: 'Montserrat', Arial, sans-serif; font-size:10px; line-height:14px; color:#666666; padding:0 20px; margin:0;" class="body-text">Av. Desembargador Moreira, 2355B - Dionísio Torres - 61170-002 - Fortaleza-CE</p>
                            </td>
                          </tr>
                          <tr>
                            <td height="10" style="font-size: 10px; line-height: 10px;">&nbsp;</td>
                          </tr>
                          <tr>
                            <td align="center" style="font-family: 'Montserrat', Arial, sans-serif; font-size:10px; line-height:14px; color:#666666; padding:0 20px;" class="body-text">
                              <p style="font-family: 'Montserrat', Arial, sans-serif; font-size:10px; line-height:14px; color:#666666; padding:0 20px; margin:0;" class="body-text">Para garantir que nossos informativos cheguem em sua caixa de entrada, adicione o email <a mailto="esace@esace.org.br" target="_blank" style="font-family: 'Montserrat', Arial, sans-serif; font-size:10px; line-height:14px; color:#364053; padding:0; margin:0;" class="body-text">esace@esace.org.br</a> ao seu catálogo de endereços.</p>
                              <p style="font-family: 'Montserrat', Arial, sans-serif; font-size:10px; line-height:14px; color:#666666; padding:0 20px; margin:0;" class="body-text">A ESACE respeita a sua privacidade e é contra o spam na rede. Se você não deseja mais receber nossos e-mails, mande um email de resposta com assunto cancelar.</p>
                            </td>
                          </tr>
                          <tr>
                            <td height="20" style="font-size: 20px; line-height: 20px;">&nbsp;</td>
                          </tr>
                        </table>
                        <!--[if (gte mso 9)|(IE)]>
                      </td>
                    </tr>
                  </table>
                  <![endif]-->
                </td>
              </tr>
              <!-- END FOOTER INFO -->

              <!-- START COPYRIGHT -->
              <tr>
                <td align="center" height="100%" valign="top" width="100%" bgcolor="#FFFFFF" style="border-top:1px solid #dddddd;">
                  <!--[if (gte mso 9)|(IE)]>
                  <table align="center" border="0" cellspacing="0" cellpadding="0" width="660">
                    <tr>
                      <td align="center" valign="top" width="660">
                        <![endif]-->
                        <table align="center" border="0" cellpadding="0" cellspacing="0" width="100%" style="max-width:660px;">
                          <tr>
                            <td height="20" style="font-size: 20px; line-height: 20px;">&nbsp;</td>
                          </tr>
                          <tr>
                            <td align="center" style="font-family: 'Montserrat', Arial, sans-serif; font-size:10px; line-height:14px; color:#666666;" class="body-text">
                              <p style="font-family: 'Montserrat', Arial, sans-serif; font-size:10px; line-height:14px; color:#666666; padding:0; margin:0;" class="body-text">&copy; 2016 ESA-CE - Todos os direitos reservados.</p>
                            </td>
                          </tr>
                          <tr>
                            <td height="20" style="font-size: 20px; line-height: 20px;">&nbsp;</td>
                          </tr>
                        </table>
                        <!--[if (gte mso 9)|(IE)]>
                      </td>
                    </tr>
                  </table>
                  <![endif]-->
                </td>
              </tr>
              <!-- END COPYRIGHT -->

            </table>
    </center>
    </td>
    </tr>
    </table>
    <div style="display:none; white-space:nowrap; font:15px courier; line-height:0;">
      &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp;
      &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp;
      &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp;
    </div>
