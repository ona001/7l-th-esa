<?php
/**
 * @file
 * Default theme implementation to display the basic html structure of a single
 * Drupal page.
 *
 * Variables:
 * - $css: An array of CSS files for the current page.
 * - $language: (object) The language the site is being displayed in.
 *   $language->language contains its textual representation.
 *   $language->dir contains the language direction. It will either be 'ltr' or
 *   'rtl'.
 * - $html_attributes:  String of attributes for the html element. It can be
 *   manipulated through the variable $html_attributes_array from preprocess
 *   functions.
 * - $html_attributes_array: An array of attribute values for the HTML element.
 *   It is flattened into a string within the variable $html_attributes.
 * - $body_attributes:  String of attributes for the BODY element. It can be
 *   manipulated through the variable $body_attributes_array from preprocess
 *   functions.
 * - $body_attributes_array: An array of attribute values for the BODY element.
 *   It is flattened into a string within the variable $body_attributes.
 * - $rdf_namespaces: All the RDF namespace prefixes used in the HTML document.
 * - $grddl_profile: A GRDDL profile allowing agents to extract the RDF data.
 * - $head_title: A modified version of the page title, for use in the TITLE
 *   tag.
 * - $head_title_array: (array) An associative array containing the string parts
 *   that were used to generate the $head_title variable, already prepared to be
 *   output as TITLE tag. The key/value pairs may contain one or more of the
 *   following, depending on conditions:
 *   - title: The title of the current page, if any.
 *   - name: The name of the site.
 *   - slogan: The slogan of the site, if any, and if there is no title.
 * - $head: Markup for the HEAD section (including meta tags, keyword tags, and
 *   so on).
 * - $styles: Style tags necessary to import all CSS files for the page.
 * - $scripts: Script tags necessary to load the JavaScript files and settings
 *   for the page.
 * - $page_top: Initial markup from any modules that have altered the
 *   page. This variable should always be output first, before all other dynamic
 *   content.
 * - $page: The rendered page content.
 * - $page_bottom: Final closing markup from any modules that have altered the
 *   page. This variable should always be output last, after all other dynamic
 *   content.
 * - $classes String of classes that can be used to style contextually through
 *   CSS.
 *
 * @see bootstrap_preprocess_html()
 * @see template_preprocess()
 * @see template_preprocess_html()
 * @see template_process()
 *
 * @ingroup templates
 */

 // var DEV
$urlFull = $GLOBALS['base_url'] . '/' . drupal_get_path('theme', 'esace_2016');

?><!DOCTYPE html>
<html<?php print $html_attributes;?><?php print $rdf_namespaces;?>>
<head>
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <meta charset="utf-8">
  <link rel="profile" href="<?php print $grddl_profile; ?>" />
  <?php print $head; ?>
  <meta name="author" content="7Links - http://7links.com.br">
  <link type="text/plain" rel="author" href="<?php print $urlFull; ?>/humans.txt">
  <meta name="HandheldFriendly" content="True">
  <meta name="MobileOptimized" content="320">
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <link rel="icon" type="image/png" href="<?php print $urlFull; ?>/image/icons/favicon.png">
  <link rel="apple-touch-icon" href="<?php print $urlFull; ?>/image/icons/apple-touch-icon.png">
  <!-- <meta name="msapplication-config" content="<?php print $urlFull; ?>/browserconfig.xml" /> -->
  <title><?php print $head_title; ?></title>
  <?php print $styles; ?>
  <!-- HTML5 element support for IE6-8 -->
  <!--[if lt IE 9]>
    <script src="//html5shiv.googlecode.com/svn/trunk/html5.js"></script>
  <![endif]-->
  <?php print $scripts; ?>
</head>
<body<?php print $body_attributes; ?>>
  <!--[if lt IE 8]>
    <p class="browserupgrade">Você está usando um navegador <strong>antigo</strong>. Por favor <a href="http://browsehappy.com/">atualize seu navegador</a> para ter uma melhor experiência.</p>
  <![endif]-->
  <div id="skip-link">
    <a href="#main-content" class="element-invisible element-focusable"><?php print t('Pular para conteúdo principal'); ?></a>
  </div>
  <?php print $page_top; ?>
  <?php print $page; ?>
  <?php print $page_bottom; ?>

  <script src="<?php print $urlFull; ?>/js/libs.js"></script>
  <script>
  ;(function($){
    'use strict';

    var $cartView, $cartBt;

    $cartView = $('.view-commerce-cart-block');
    $cartBt   = $('.line-item-quantity');

    $cartBt.on( 'click', function(e) {
      e.preventDefault();

      $(this).toggleClass( 'active' );
      $(this).closest('.view-commerce-cart-block').toggleClass('active');
    });

    // fechar dropdowmn com clique fora da caixa
   $( document ).click(function( e ) {
     var target = e.target.parentNode;

     if ( !$(target).is( '#block-commerce-cart-cart' ) && !$( target ).parents().is( '#block-commerce-cart-cart' ) ) {
       $( '.view-commerce-cart-block' ).removeClass( 'active' );
     }
   });



    // ancor animate to top
    $( 'a[href*="#"]' ).click( function() {
			if ( location.pathname.replace(/^\//,'') == this.pathname.replace(/^\//,'') && location.hostname == this.hostname ){
				var $target = $(this.hash);

				$target = $target.length && $target || $('[name=' + this.hash.slice(1) +']');

				if ($target.length) {
					var targetOffset = $target.offset().top;
					$('html, body').animate({scrollTop: targetOffset}, 2000 );
					return false;
				}
			}
    });

    // external links, alternative for targer _blank
    $( 'a[rel*=external]' ).click(
			function() {
				window.open( this.href );
				return false;
			}
		);

  })(jQuery);
  </script>
</body>
</html>
