# 7Links - Projeto ESA

* www.esace.org.br
* Versão 0.0.1
* Data: 13/04/2016
* Standards: HTML(X), CSS(X)
* Components: Drupal, SASS, JS/jQuery
* Software: Brackets, Atom, Photoshop


## O projeto

Site de e-commerce de cursos online para ESA - Escola superior de Advocacia.


## Autor

**7Links - Soluções Web**

* Site - [www.7link.com.br](http://7links.com.br)
* E-mail - contato@7links.com.br


## Equipe

* Desing de interface + Front-end - [Jonas Sousa](https://www.behance.net/onasousa)

* Development Director [Ronan Ribeiro](http://7links.com.br)
